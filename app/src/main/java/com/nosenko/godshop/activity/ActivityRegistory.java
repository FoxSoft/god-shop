package com.nosenko.godshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.internal.IdTokenListener;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.internal.InternalTokenResult;
import com.nosenko.godshop.MainActivity;
import com.nosenko.godshop.R;
import com.nosenko.godshop.helper.DataUntill;
import com.nosenko.godshop.model.StatusUser;
import com.nosenko.godshop.model.User;
import com.nosenko.godshop.ui.ToastGS;

import java.util.HashMap;
import java.util.Map;


public class ActivityRegistory extends AppCompatActivity {
    private EditText nameEdit, passEdit, emailEdit;
    private Button butReg;
    private FirebaseAuth mAuth;
    private User.gender genderUser;
    private DatabaseReference database;
    private RadioButton rbW, rbM;
    private View backButton;
    private ProgressDialog progressDialog;
    private ImageView visibilityPas;
    FirebaseFirestore firebaseFirestore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registory);

        nameEdit = findViewById(R.id.name);
        emailEdit = findViewById(R.id.email);
        passEdit = findViewById(R.id.pass);
        backButton = findViewById(R.id.backButton);

        rbM = findViewById(R.id.radioButtonM);
        rbW = findViewById(R.id.radioButtonW);

        butReg = findViewById(R.id.but_reg);
        visibilityPas = findViewById(R.id.hidePas);

        inintRB();

        passEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("controlInput", count + " " + start + " " + before);
                if (start < 6)
                    passEdit.setBackground(getResources().getDrawable(R.drawable.style_error_input));
                else
                    passEdit.setBackground(getResources().getDrawable(R.drawable.style_back_edit));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        butReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(nameEdit.getText().toString()) || !TextUtils.isEmpty(emailEdit.getText().toString()) || !TextUtils.isEmpty(passEdit.getText().toString())) {
                    progressDialog.show();
                    registerUser(nameEdit.getText().toString(), emailEdit.getText().toString(), passEdit.getText().toString());

                    Map<String, String> map = new HashMap<>();
                    map.put("email", emailEdit.getText().toString());
                    map.put("pass", passEdit.getText().toString());

                    DataUntill.setPref(getApplicationContext(), map);
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                finishAffinity();
            }
        });

        visibilityPas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getAlpha() == 1) {
                    //view
                    passEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    visibilityPas.setAlpha(0.5f);

                } else {
                    //hide
                    passEdit.setInputType(InputType.TYPE_CLASS_TEXT);
                    visibilityPas.setAlpha(1f);
                }
            }
        });

        mAuth = FirebaseAuth.getInstance();

        mAuth.getAccessToken(true).addOnSuccessListener(new OnSuccessListener<GetTokenResult>() {
            @Override
            public void onSuccess(GetTokenResult getTokenResult) {
                Toast.makeText(getApplicationContext(), getTokenResult.getToken(), Toast.LENGTH_LONG).show();
                Log.d("token", getTokenResult.getSignInProvider());
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Регистрация");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    private void inintRB() {
        if (rbM.isChecked())
            genderUser = User.gender.man;
        else
            genderUser = User.gender.wooman;

        rbW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                rbW.setChecked(true);
                rbM.setChecked(false);
                compoundButton.setChecked(true);
                genderUser = User.gender.wooman;

            }
        });

        rbM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                rbM.setChecked(true);
                rbW.setChecked(false);
                compoundButton.setChecked(true);
                genderUser = User.gender.man;

            }
        });

    }

    private void registerUser(final String name, final String email, String pass) {
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            User userCurrent = new User();
                            userCurrent.setUID(currentUser.getUid());
                            userCurrent.setName(name);
                            userCurrent.setEmail(email);
                            userCurrent.setStatus("Я пользователь DevChat!");
                            userCurrent.setDateOfBirth("");
                            userCurrent.setIdLogo("");
                            userCurrent.setGender(genderUser);
                            userCurrent.setAge("");
                            userCurrent.setThumb("");
                            userCurrent.setStatusNet(StatusUser.StatusNet.ONLINE);
                            userCurrent.setLastSeen("");
                            userCurrent.setTokenId("");

                            firebaseFirestore = FirebaseFirestore.getInstance();

                            firebaseFirestore.collection("users").document(currentUser.getUid()).set(userCurrent).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d("FirebaseFirestore", "DocumentSnapshot added with ID: ");

                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    finish();
                                    progressDialog.dismiss();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w("FirebaseFirestore", "Error adding document", e);
                                    progressDialog.hide();

                                    Toast.makeText(getApplicationContext(), "Не удалось создать аккаунт, повторите снова", Toast.LENGTH_SHORT).show();
                                }
                            });

                            progressDialog.dismiss();
                        } else {
                            progressDialog.hide();
                            Log.w("regUser", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Не удалось создать аккаунт, повторите снова", Toast.LENGTH_SHORT).show();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("register", e.getMessage());
                ToastGS toastGS = new ToastGS(getApplicationContext());
                toastGS.show(e.getMessage());
            }
        });
    }


}
