package com.nosenko.godshop.activity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.nosenko.godshop.MainActivity;
import com.nosenko.godshop.R;
import com.nosenko.godshop.helper.DataUntill;
import com.nosenko.godshop.ui.ToastGS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActivityLogin extends AppCompatActivity {
    private Button createAccount;
    private EditText emailEdit, passEdit;
    private Button buttonLogin;
    private View backButton;
    private ProgressDialog progressDialog;
    private TextView replacePassword;
    private ImageView visibilityPas;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        backButton = findViewById(R.id.backButton);
        createAccount = findViewById(R.id.create_acunt);
        emailEdit = findViewById(R.id.email);
        passEdit = findViewById(R.id.pass);
        buttonLogin = findViewById(R.id.but_log);
        replacePassword = findViewById(R.id.replasePass);
        visibilityPas = findViewById(R.id.hidePas);

        mAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Авторизация");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(emailEdit.getText().toString()) || !TextUtils.isEmpty(passEdit.getText().toString())) {
                    progressDialog.show();
                    mAuth.signInWithEmailAndPassword(emailEdit.getText().toString(), passEdit.getText().toString()).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                        @Override
                        public void onSuccess(AuthResult authResult) {
                            Map<String, String> map = new HashMap<>();
                            map.put("email", emailEdit.getText().toString());
                            map.put("pass", passEdit.getText().toString());

                            DataUntill.setPref(getApplicationContext(), map);

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                            progressDialog.dismiss();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            ToastGS toastGS = new ToastGS(getApplicationContext());
                            toastGS.show(e.getMessage());
                        }
                    });
                } else {
                    ToastGS toastGS = new ToastGS(getApplicationContext());
                    toastGS.show("Проверте правильность данных");
                }
            }
        });

        visibilityPas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getAlpha() == 1) {
                    //view
                    passEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    visibilityPas.setAlpha(0.5f);

                } else {
                    //hide
                    passEdit.setInputType(InputType.TYPE_CLASS_TEXT);
                    visibilityPas.setAlpha(1f);
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });

        replacePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityLogin.this, ActivityReplacePass.class);
                startActivity(intent);
            }
        });

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityLogin.this, ActivityRegistory.class);

                Pair[] pairs = new Pair[6];
                pairs[0] = new Pair<View, String>(buttonLogin, "butTranslation");
                pairs[1] = new Pair<View, String>(createAccount, "createTranslation");
                pairs[2] = new Pair<View, String>(passEdit, "passTranslation");
                pairs[3] = new Pair<View, String>(emailEdit, "emailTranslation");
                pairs[4] = new Pair<View, String>(backButton, "backTranslation");
                pairs[5] = new Pair<View, String>(visibilityPas, "visibilityPasTranslation");


                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(ActivityLogin.this, pairs);
                startActivity(intent, options.toBundle());
            }


        });

        getCashSetting();

    }

    private void getCashSetting() {
        List<String> keys = new ArrayList<>();
        keys.add("email");
        keys.add("pass");

        Map<String, String> data = DataUntill.getPref(getApplicationContext(), keys);

        if (!data.isEmpty()) {
            emailEdit.setText(data.get("email"));
            passEdit.setText(data.get("pass"));
        }
    }

}
