package com.nosenko.godshop.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nosenko.godshop.R;
import com.nosenko.godshop.adapter.AdapterOptionOrders;
import com.nosenko.godshop.model.OptionOrders;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentProfile extends Fragment {

    private View view;
    private List<OptionOrders> optionOrders;
    private RecyclerView recyclerOptOrders;
    private AdapterOptionOrders adapterOptionOrders;
    private LinearLayoutManager manager;


    public FragmentProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_profile, container, false);

        recyclerOptOrders = view.findViewById(R.id.recItemsOrders);
        manager = new LinearLayoutManager(getContext());
        recyclerOptOrders.setLayoutManager(manager);

        optionOrders = new ArrayList<>();
        optionOrders.add(new OptionOrders("Полученные", 105));
        optionOrders.add(new OptionOrders("Отправленные", 98));
        optionOrders.add(new OptionOrders("Ожидают отправку", 6));
        optionOrders.add(new OptionOrders("Ожидают оплату", 2));
        optionOrders.add(new OptionOrders("Ожидают отзыв", 105));
        optionOrders.add(new OptionOrders("Споры по заказам", 1));

        adapterOptionOrders = new AdapterOptionOrders(getContext(), optionOrders);
        recyclerOptOrders.setAdapter(adapterOptionOrders);

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();

    }


}
