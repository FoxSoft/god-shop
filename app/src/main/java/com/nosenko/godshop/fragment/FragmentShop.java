package com.nosenko.godshop.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nosenko.godshop.R;
import com.nosenko.godshop.view.CardView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentShop extends Fragment {

    private View view;


    public FragmentShop() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_shop, container, false);
        CardView cardView = view.findViewById(R.id.card);
        cardView.setPrice(12569);
        cardView.setTitle("Телевизор Asus GT-3600 Pro HD");
        cardView.setLabelText("-70%");
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

    }


}
