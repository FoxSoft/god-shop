package com.nosenko.godshop.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nosenko.godshop.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    private View view;


    public FragmentHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_home, container, false);



        return view;
    }



    @Override
    public void onStart() {
        super.onStart();

    }


}
