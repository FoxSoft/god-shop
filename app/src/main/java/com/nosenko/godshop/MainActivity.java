package com.nosenko.godshop;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.nosenko.godshop.adapter.AdapterPage;
import com.nosenko.godshop.fragment.FragmentHome;
import com.nosenko.godshop.fragment.FragmentProfile;
import com.nosenko.godshop.fragment.FragmentShop;
import com.nosenko.godshop.helper.HelperTabTitle;
import com.nosenko.godshop.ui.ToastGS;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private HelperTabTitle helperTabTitle1, helperTabTitle2, helperTabTitle3;
    private ViewPager viewPager;
    private AdapterPage adapterPager;
    private TabLayout tabLayout;
    private FragmentManager fragmentManager;
    private ImageView searchButton;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
//make translucent statusBar on kitkat devices
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        fragmentManager = getSupportFragmentManager();
        searchButton = findViewById(R.id.searchBut);
        viewPager = findViewById(R.id.pager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabL);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setCustomView(R.layout.title_tab);
        tabLayout.getTabAt(1).setCustomView(R.layout.title_tab);
        tabLayout.getTabAt(2).setCustomView(R.layout.title_tab);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                view.setAlpha(1f);
                TextView title = view.findViewById(R.id.title);
                title.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                view.setAlpha(0.8f);
                TextView title = view.findViewById(R.id.title);
                title.setTextColor(getResources().getColor(R.color.silver_b));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        searchButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                auth = FirebaseAuth.getInstance();
//                auth.signOut();
//                Toast.makeText(getApplicationContext(), "click", Toast.LENGTH_SHORT).show();
//            }
//        });
        createHeader();

        auth = FirebaseAuth.getInstance();
        Toast.makeText(getApplicationContext(), auth.getUid(), Toast.LENGTH_SHORT).show();

        FirebaseFirestore db = FirebaseFirestore.getInstance();



// Add a new document with a generated ID
//        db.collection("users")
//                .add(user)
//                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
//                    @Override
//                    public void onSuccess(DocumentReference documentReference) {
//                        Log.d("777", "DocumentSnapshot added with ID: " + documentReference.getId());
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Log.w("777", "Error adding document", e);
//                    }
//                });

//        Map<String, Object> user = new HashMap<>();
//        user.put("first", "Ada");
//        user.put("last", "Lovelace");
//        user.put("born", 1815);
//
//        db.collection("users").document(auth.getUid()).set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//                Log.d("FirebaseFirestore", "DocumentSnapshot added with ID: ");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                Log.w("FirebaseFirestore", "Error adding document", e);
//            }
//        });
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapterPager = new AdapterPage(getSupportFragmentManager());
        adapterPager.addFragment(new FragmentHome(), "Главная");
        adapterPager.addFragment(new FragmentShop(), "Корзина");
        adapterPager.addFragment(new FragmentProfile(), "Профиль");
        viewPager.setAdapter(adapterPager);
    }

    private void createHeader() {
        helperTabTitle1 = new HelperTabTitle(tabLayout.getTabAt(0).getCustomView());
        helperTabTitle1.setNumberBadge(0);
        helperTabTitle1.setImg(R.drawable.home);
        helperTabTitle1.setTitle("Главная");

        helperTabTitle2 = new HelperTabTitle(tabLayout.getTabAt(1).getCustomView());
        helperTabTitle2.setImg(R.drawable.basket);
        helperTabTitle2.setNumberBadge(7);
        helperTabTitle2.setTitle("Корзина");

        helperTabTitle3 = new HelperTabTitle(tabLayout.getTabAt(2).getCustomView());
        helperTabTitle3.setImg(R.drawable.profile);
        helperTabTitle3.setNumberBadge(0);
        helperTabTitle3.setTitle("Профиль");
    }

}
